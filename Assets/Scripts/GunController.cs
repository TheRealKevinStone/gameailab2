﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour 
{
	private StateMachine fsm = new StateMachine();

	void Start () 
	{
		GunStateReady stateReady 				= fsm.AddState<GunStateReady> ();
		GunStateFire stateFire 					= fsm.AddState<GunStateFire> ();
		GunStateNoFireFX stateNoFireFX 			= fsm.AddState<GunStateNoFireFX> ();
		GunStateOutOfAmmo stateOutOfAmmo 		= fsm.AddState<GunStateOutOfAmmo> ();
		GunStateRefireDelay stateRefireDelay 	= fsm.AddState<GunStateRefireDelay> ();
		GunStateReloading stateReloading 		= fsm.AddState<GunStateReloading> ();
	}
	
	void Update () 
	{
	
	}
}
