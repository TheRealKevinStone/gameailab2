﻿using UnityEngine;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("StateMachine")]
public class FSMState : Object 
{
	private StateMachine owner;

	internal FSMState()
	{

	}

	public virtual void Update () 
	{
	
	}
}
