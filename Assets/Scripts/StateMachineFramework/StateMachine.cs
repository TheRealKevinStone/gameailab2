﻿using UnityEngine;
using System.Collections;

public class StateMachine : Object 
{
	public TextMesh debugText;

	public StateMachine()
	{

	}

	public TStateType AddState<TStateType> () where TStateType : new()
	{
		TStateType state = new TStateType ();
		return state;
	}

	void Start()
	{
		SetDebugText ("Ready");
	}

	void Update()
	{

	}

	void SetDebugText(string newText)
	{
		if (debugText) 
		{
			debugText.text = newText;
		} 
	}
}
